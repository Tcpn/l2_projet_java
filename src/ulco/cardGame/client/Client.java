package ulco.cardGame.client;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.interfaces.Board;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.players.PokerPlayer;
import ulco.cardGame.server.SocketServer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {


    public static void main(String[] args) {

        // Current use full variables
        try {

            // Create a connection to the server socket on the server application
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;

            Object answer;
            String username;
            Game game;

            Scanner scanner = new Scanner(System.in);

            System.out.print("Please select your username : ");
            username = scanner.nextLine();

            socket = new Socket(host.getHostName(), SocketServer.PORT);

            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(username);

            do {
                // Read and display the response message sent by server application
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                answer = ois.readObject();

                // depending of object type, we can manage its data
                if (answer instanceof String) {
                    System.out.println(answer.toString());

                    if(((String) answer).contains("you have to play")){
                        ObjectOutputStream action = new ObjectOutputStream(socket.getOutputStream());
                        action.writeObject(username + " plays a card");
                    }
                    if(((String) answer).contains("plays a card")){
                        ObjectOutputStream action = new ObjectOutputStream(socket.getOutputStream());
                        action.writeObject(username + " remove the first card");
                    }

                    if (((String) answer).contains("valid Coin to play")){
                        System.out.print("-> ");
                        String colour = scanner.nextLine();
                        ObjectOutputStream action = new ObjectOutputStream(socket.getOutputStream());
                        action.writeObject(colour);
                    }

                }
                if (answer instanceof Game) {
                    game = (Game) answer;
                    game.displayState();
                }
                if(answer instanceof Board){
                    ((Board)answer).displayState();
                }

                if (answer instanceof Card){
                    ObjectOutputStream action = new ObjectOutputStream(socket.getOutputStream());
                    action.writeObject(answer);
                }

                if (answer instanceof PokerPlayer){
                    ((PokerPlayer) answer).displayHand();
                }
                if (answer instanceof Coin){
                    ObjectOutputStream action = new ObjectOutputStream(socket.getOutputStream());
                    action.writeObject(answer);
                }
            } while (!answer.equals("END"));

            // close the socket instance connection
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}