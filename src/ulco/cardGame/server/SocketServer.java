package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Server Game management with use of Singleton instance creation
 */
public class SocketServer {

    private ServerSocket server;
    protected Game game; // game will be instantiate later
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    private SocketServer() {
        try {
            server = new ServerSocket(PORT);
            playerSockets = new HashMap<>();
            playerConstructor = null;

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        SocketServer server = new SocketServer();
        server.run();
    }

    private void run() {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Please select a game (Battle / Poker) : ");
        String gameType = scanner.nextLine();

        if (gameType. equals("Battle") || gameType.equals("Poker")){

            try {

                if (gameType.equals("Battle")) game = new CardGame("Battle", 3, "resources/games/cardGame.txt");
                else  game = new PokerGame("Poker", 3, 3, "resources/games/pokerGame.txt");


                // Game Loop
                System.out.println("Waiting for new player for " + game);

                // add each player until game is ready (or number of max expected player is reached)
                // Waiting for the socket entrance
                do {

                    Socket socket = server.accept();

                    // read socket Data
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    String playerUsername = (String) ois.readObject();

                    // Create player instance
                    Player player = null;
                    if (gameType.equals("Battle")) player = new CardPlayer(playerUsername);
                    else  player = new PokerPlayer(playerUsername);


                    if (game.addPlayer(player, socket)){

                        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                        oos.writeObject(game);

                        // Tell to other players that new player is connected
                        for (Socket playerSocket : playerSockets.values()) {

                            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                            playerOos.writeObject("Now connected: " + player.getName());
                        }

                        // Store player's socket in dictionary (Map)
                        playerSockets.put(player, socket);

                        System.out.println(player.getName() + " is now connected...");
                    }

                    else{

                        System.out.println("ERROR WHILE CONNECTING");
                        ObjectOutputStream oos = new ObjectOutputStream ( socket.getOutputStream () ) ;
                        oos.writeObject ("END") ;
                    }


                } while (!game.isStarted());

                // run the whole game using sockets
                Player gameWinner = game.run(playerSockets);

                for (Socket playerSocket : playerSockets.values()) {
                    ObjectOutputStream playerOos1 = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos1.writeObject("\n---> " + gameWinner.getName() + " won the game !!! ");

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("END");
                }
                System.out.println("\n--->" + gameWinner.getName() + " won the game !!! ");

                // Close each socket when game is finished
                for (Socket socket : playerSockets.values()) {
                    socket.close();
                }

                game.removePlayers();

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }

        }

    }
}

