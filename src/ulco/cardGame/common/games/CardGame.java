package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * CardGame which extend BoardGame
 * BoardGame need to implement Game interface methods
 */
public class CardGame extends BoardGame {

    private List<Card> cards;
    private Integer numberOfRounds;

    /**
     * Enable constructor of CardGame
     * - Name of the game
     * - Maximum number of players of the Game
     *  - Filename of current Game
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);

        this.board = new CardBoard();
        this.numberOfRounds = 0;
    }

    @Override
    public void initialize(String filename) {

        this.cards = new ArrayList<>();

        // Here initialize the list of Cards
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] dataValues = data.split(";");

                // get Card value
                Integer value = Integer.valueOf(dataValues[1]);
                this.cards.add(new Card(dataValues[0], value, true));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public Player run(Map<Player, Socket> playerSocket) {

        Player gameWinner = null;
        Object answer;

        // prepare to distribute card to each player
        Collections.shuffle(cards);

        int playerIndex = 0;

        for (Card card : cards) {

            players.get(playerIndex).addComponent(card);
            card.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }

        for (Map.Entry<Player, Socket> socketPlayer : playerSocket.entrySet()){


            try {
                ObjectOutputStream oos = new ObjectOutputStream ( socketPlayer.getValue().getOutputStream() ) ;
                oos.writeObject ( socketPlayer.getKey().getName() + " has " + socketPlayer.getKey().getComponents().size() + " cards" );

                ObjectOutputStream oos2 = new ObjectOutputStream ( socketPlayer.getValue().getOutputStream() ) ;
                oos2.writeObject ( this );
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

        // while each player can play
        while (!this.end()) {

            Map<Player, Card> playedCard = new HashMap<>();


            for (Map.Entry<Player, Socket> socketPlayer : playerSocket.entrySet()) {

                if (socketPlayer.getKey().isPlaying()){

                    try{
                        ObjectOutputStream oos = new ObjectOutputStream(socketPlayer.getValue().getOutputStream());
                        oos.writeObject("[" + socketPlayer.getKey().getName() + "] you have to play...");
                    }catch (IOException e){
                        e.printStackTrace();
                    }



                for (Map.Entry<Player, Socket> state : playerSocket.entrySet()){
                    if (!state.getKey().equals(socketPlayer.getKey())){
                        try{
                            ObjectOutputStream oos = new ObjectOutputStream(state.getValue().getOutputStream());
                            oos.writeObject("Waiting for " + socketPlayer.getKey().getName() + " to play...");
                        }catch (IOException e){
                            e.printStackTrace();
                        }
                    }
                }



                    try{
                        ObjectInputStream ois = new ObjectInputStream(socketPlayer.getValue().getInputStream());
                        answer = ois.readObject();

                        if (answer instanceof String){
                            System.out.println(answer);
                            if (((String) answer).contains("plays a card")){
                                socketPlayer.getKey().play(socketPlayer.getValue());
                            }
                        }

                        ObjectInputStream ois2 = new ObjectInputStream(socketPlayer.getValue().getInputStream());
                        answer = ois2.readObject();

                        if (answer instanceof Card){
                            socketPlayer.getKey().removeComponent((Component) answer);
                            board.addComponent((Component) answer);
                            playedCard.put(socketPlayer.getKey(), (Card) answer);
                            for (Map.Entry<Player, Socket> playerCard : playerSocket.entrySet()) {

                                ObjectOutputStream oos = new ObjectOutputStream(playerCard.getValue().getOutputStream());
                                oos.writeObject("-> " + socketPlayer.getKey().getName() + " has played " + ((Card) answer).getName() + "\n");

                            }
                        }

                    }catch (IOException e){
                        e.printStackTrace();
                    }catch(ClassNotFoundException e){
                        e.printStackTrace();
                    }
                }

            }

            for (Map.Entry<Player, Socket> socketPlayer : playerSocket.entrySet()){
                try{
                    ObjectOutputStream oos1 = new ObjectOutputStream(socketPlayer.getValue().getOutputStream());
                    oos1.writeObject(board);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            board.displayState();

            // Check which player has win
            int bestValueCard = 0;

            List<Player> possibleWinner = new ArrayList<>();
            List<Card> possibleWinnerCards = new ArrayList<>();

            for (Card card : playedCard.values()) {
                if (card.getValue() >= bestValueCard)
                    bestValueCard = card.getValue();
            }

            // check if equality
            for(Map.Entry<Player, Card> entry : playedCard.entrySet()){

                Card currentCard = entry.getValue();

                if (currentCard.getValue() >= bestValueCard) {

                    possibleWinner.add(entry.getKey());
                    possibleWinnerCards.add(currentCard);
                }
            }

            // default winner index
            int winnerIndex = 0;

            // Random choice if equality is reached
            if (possibleWinner.size() > 1){
                Random random = new Random();
                winnerIndex = random.nextInt(possibleWinner.size() - 1);
            }

            Player roundWinner = possibleWinner.get(winnerIndex);
            Card winnerCard = possibleWinnerCards.get(winnerIndex);

            for (Map.Entry<Player, Socket> winnerRound : playerSocket.entrySet()){
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(winnerRound.getValue().getOutputStream());
                    oos.writeObject("-> " + roundWinner.getName() + " won the round with " + winnerCard + "\n");
                }catch (IOException e){
                    e.printStackTrace();
                }
            }

            // Update Game state
            for (Card card : playedCard.values()) {

                // remove from previous player
                roundWinner.addComponent(card);
                card.setPlayer(roundWinner);
            }

            // clear board state
            board.clear();

            // Check players State
            for(Map.Entry<Player,Socket> socketPlayer : playerSocket.entrySet()){

                if(socketPlayer.getKey().getScore() == 0){
                    socketPlayer.getKey().canPlay(false);
                }

                if(socketPlayer.getKey().getScore() == cards.size()){
                    socketPlayer.getKey().canPlay(false);
                    gameWinner = socketPlayer.getKey();
                }
            }

            // Display Game state
            for (Map.Entry<Player, Socket> playerSockets : playerSocket.entrySet()){
                try {
                    ObjectOutputStream oos = new ObjectOutputStream(playerSockets.getValue().getOutputStream());
                    oos.writeObject(this);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            this.displayState();

            // shuffle player hand every n rounds
            this.numberOfRounds += 1;

            if (this.numberOfRounds % 10 == 0) {
                for (Player player : players){
                    player.shuffleHand();
                }

        }
        }
        return gameWinner;
    }

    @Override
    public boolean end() {
        // check if it's the end of the game
        endGame = true;
        for (Player player : players) {
            if (player.isPlaying()) {
                endGame = false;
            }
        }
        return endGame;
    }

    @Override
    public String toString() {
        return "CardGame{" +
                "name='" + name + '\'' +
                '}';
    }
}
